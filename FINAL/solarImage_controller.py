# NETID's jporubci tcoverst

import random
import json
import cherrypy
from solarSystems_library import _solarSystem_database

class SolarImageController(object):
    def __init__(self, mkdb=None):
        if mkdb is None:
            self.mkdb = _solarSystem_database()
        else:
            self.mkdb = mkdb

        self.mkdb.load_solarImages("solarImages.dat")

    def GET_KEY(self, solarImage_id):
        output = {'result':'success'}
        try:
            solarImage_id = int(solarImage_id)
            solarImage = self.mkdb.get_solarImage(solarImage_id)
            print(solarImage)
            if solarImage is not None:
                output['solarImage_id']         = solarImage_id
                output['Name']         = solarImage[0]
                output['Image']         = solarImage[1]
                output['solarImage_attributes'] = solarImage
            else:
                output['result']            = 'error'
                output['message']           = 'solarImage not found'

        except Exception as ex:
            output['result']                = 'error'
            output['message']               = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, solarImage_id):
        output = {'result': 'success'}
        solarImage_id = int(solarImage_id)
        data_str = cherrypy.request.body.read().decode('utf-8')
        data_json = json.loads(data_str)
        solarImage_attributes = data_json['solarImage_attributes']

        self.mkdb.set_solarImage(solarImage_id, solarImage_attributes)

        return json.dumps(output)

    def DELETE_KEY(self, solarImage_id):
        output = {'result': 'success'}
        try:
            solarImage_id = int(solarImage_id)
            self.mkdb.delete_solarImage(solarImage_id)

        except Exception as ex:
            output['result']    = 'failure'
            output['message']   = str(ex)

        return json.dumps(output)

    def GET_INDEX(self):
        output = {'result': 'success'}
        output['solarImages'] = []
        try:
            for solarImage_id in self.mkdb.get_solarImages():
                data_str = self.GET_KEY(solarImage_id)
                data_json = json.loads(data_str)
                data_json.pop('result')
                output['solarImages'].append(data_json)

        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)

    def POST_INDEX(self):
        output = {'result': 'success'}
        data_str = cherrypy.request.body.read().decode('utf-8')
        data_json = json.loads(data_str)

        try:
            solarImages = sorted(list(self.mkdb.get_solarImages()))
            newID = int(solarImages[-1]) + 1
            self.mkdb.solarImages[newID] = data_json.get('solarImage_attributes')
            output['solarImage_id'] = newID

        except Exception as ex:
            output['result']    = 'failure'
            output['message']   = str(ex)

        return json.dumps(output)

    def DELETE_INDEX(self):
        output = {'result': 'success'}
        self.mkdb.solarImages.clear()

        return json.dumps(output)
