import unittest
import requests
import json

class TestSolarImages(unittest.TestCase):

    SITE_URL = 'http://localhost:8080' # replace with your port number and
    print("testing for server: " + SITE_URL)
    SOLARIMAGE_URL = SITE_URL + '/solarImage/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_solarImages_get_key(self):
        self.reset_data()
        solarImage_id = 32
        r = requests.get(self.SOLARIMAGE_URL + str(solarImage_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['solarImage_attributes'], ["Tethys", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/PIA18317-SaturnMoon-Tethys-Cassini-20150411.jpg/800px-PIA18317-SaturnMoon-Tethys-Cassini-20150411.jpg"])

    def test_solarImages_put_key(self):
        self.reset_data()
        solarImage_id = 18

        r = requests.get(self.SOLARIMAGE_URL + str(solarImage_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['solarImage_attributes'], ["Pluto", "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Pluto_in_True_Color_-_High-Res.jpg/800px-Pluto_in_True_Color_-_High-Res.jpg"])

        m = {}
        m['solarImage_attributes'] = ["PlanetX", "planetX.jpg"]
        r = requests.put(self.SOLARIMAGE_URL + str(solarImage_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SOLARIMAGE_URL + str(solarImage_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['solarImage_attributes'], m['solarImage_attributes'])

    def test_solarImages_delete_key(self):
        self.reset_data()
        solarImage_id = 29

        m = {}
        r = requests.delete(self.SOLARIMAGE_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SOLARIMAGE_URL + str(solarImage_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')
        #self.assertEqual(resp['message'], 'solarImage not found')

if __name__ == "__main__":
    unittest.main()
