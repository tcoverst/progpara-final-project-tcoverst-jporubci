# NETID's jporubci tcoverst

import random
import json
import cherrypy
from solarSystems_library import _solarSystem_database

class SolarSystemController(object):
    def __init__(self, mkdb=None):
        if mkdb is None:
            self.mkdb = _solarSystem_database()
        else:
            self.mkdb = mkdb

        self.mkdb.load_solarSystems("solarSystems.dat")

    def GET_KEY(self, solarSystem_id):
        output = {'result':'success'}
        try:
            print(solarSystem_id)
            solarSystem_id = int(solarSystem_id)
            solarSystem = self.mkdb.get_solarSystem(solarSystem_id)
            print(solarSystem)
            if solarSystem is not None:
                output['solarSystem_id']           = solarSystem_id
                output['Name']   = solarSystem[0]
                output['Radius(km)']   = solarSystem[1]
                output['Volume(km^3)']   = solarSystem[3]
                output['Mass(kg)']   = solarSystem[5]
                output['SurfaceArea(km^2)']   = solarSystem[7]
                output['Density(g/cm^3)']   = solarSystem[9]
                output['Gravity(m/s^2)']   = solarSystem[10]
                output['Type']   = solarSystem[12]
                output['Discovered']   = solarSystem[13]
                output['solarSystem_attributes']   = solarSystem
            else:
                output['result']    = 'error'
                output['message']   = 'solarSystem not found'

        except Exception as ex:
            output['result']        = 'error'
            output['message']       = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, solarSystem_id):
        output = {'result': 'success'}
        solarSystem_id = int(solarSystem_id)
        data_str = cherrypy.request.body.read().decode('utf-8')
        data_json = json.loads(data_str)
        solarSystem_attributes = data_json['solarSystem_attributes']

        self.mkdb.set_solarSystem(solarSystem_id, solarSystem_attributes)

        return json.dumps(output)

    def DELETE_KEY(self, solarSystem_id):
        output = {'result': 'success'}
        try:
            solarSystem_id = int(solarSystem_id)
            self.mkdb.delete_solarSystem(solarSystem_id)

        except Exception as ex:
            output['result']    = 'failure'
            output['message']   = str(ex)

        return json.dumps(output)

    def GET_INDEX(self):
        output = {'result': 'success'}
        output['solarSystems'] = []
        try:
            for solarSystem_id in self.mkdb.get_solarSystems():
                data_str = self.GET_KEY(solarSystem_id)
                data_json = json.loads(data_str)
                data_json.pop('result')
                output['solarSystems'].append(data_json)

        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)

    def POST_INDEX(self):
        output = {'result': 'success'}
        data_str = cherrypy.request.body.read().decode('utf-8')
        data_json = json.loads(data_str)

        try:
            solarSystems = sorted(list(self.mkdb.get_solarSystems()))
            newID = int(solarSystems[-1]) + 1
            self.mkdb.solarSystems[newID] = data_json.get('solarSystem_attributes')
            output['solarSystem_id'] = newID

        except Exception as ex:
            output['result']    = 'failure'
            output['message']   = str(ex)

        return json.dumps(output)

    def DELETE_INDEX(self):
        output = {'result': 'success'}
        self.mkdb.solarSystems.clear()

        return json.dumps(output)
