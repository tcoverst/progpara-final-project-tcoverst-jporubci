# NETID's jporubci tcoverst

import routes
import cherrypy
from solarSystem_controller import SolarSystemController
from solarImage_controller import SolarImageController
from reset_controller import ResetController
from solarSystems_library import _solarSystem_database

def start_service():
    mkdb = _solarSystem_database()
    solarSystemController = SolarSystemController(mkdb=mkdb)
    solarImageController = SolarImageController(mkdb=mkdb)
    resetController = ResetController(mkdb=mkdb)

    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    dispatcher.connect('solarSystem_get_key','/solarSystem/:solarSystem_id', controller=solarSystemController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('solarSystem_put_key','/solarSystem/:solarSystem_id', controller=solarSystemController, action='PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('solarSystem_get_all','/solarSystem/', controller=solarSystemController, action='GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('solarSystem_post_new','/solarSystem/', controller=solarSystemController, action='POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('solarSystem_delete_all','/solarSystem/', controller=solarSystemController, action='DELETE_INDEX', conditions=dict(method=['DELETE']))
    dispatcher.connect('solarImage_get_key','/solarImage/:solarImage_id', controller=solarImageController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('solarImage_put_key','/solarImage/:solarImage_id', controller=solarImageController, action='PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('solarImage_get_all','/solarImage/', controller=solarImageController, action='GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('solarImage_post_new','/solarImage/', controller=solarImageController, action='POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('solarImage_delete_all','/solarImage/', controller=solarImageController, action='DELETE_INDEX', conditions=dict(method=['DELETE']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # default OPTIONS handler for CORS, all direct to the same place
    dispatcher.connect('solarSystem_options', '/solarSystem/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('solarSystem_key_options', '/solarSystem/:solarSystem_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('solarImage_options', '/solarImage/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('solarImage_key_options', '/solarImage/:solarImage_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:reset_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    #set up configuration
    conf = {
        'global' : {
            'server.socket_host' : 'localhost', #'student04.cse.nd.edu',
            'server.socket_port' : 8080
            },
        '/' : {
            'request.dispatch' : dispatcher,
            'tools.CORS.on' : True # configuration for CORS
            }
    }

    #update with new configuration
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf) # create app
    cherrypy.quickstart(app)    # start app

# class for CORS
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
    start_service()
