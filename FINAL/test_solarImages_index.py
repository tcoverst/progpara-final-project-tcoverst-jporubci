import unittest
import requests
import json

class TestSolarImagesIndex(unittest.TestCase):

    SITE_URL = 'http://localhost:8080' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    SOLARIMAGE_URL = SITE_URL + '/solarImage/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_solarImages_index_get(self):
        self.reset_data()
        r = requests.get(self.SOLARIMAGE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testsolarImage = {}
        solarImages = resp['solarImages']
        for solarImage in solarImages:
            if solarImage['solarImage_id'] == 4:
                testsolarImage = solarImage

        self.assertEqual(testsolarImage['solarImage_attributes'], ["Saturn", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Ringworld_Waiting.jpg/1024px-Ringworld_Waiting.jpg"])

    def test_solarImages_index_post(self):
        self.reset_data()

        m = {}
        m['solarImage_attributes'] = ["PlanetX", "planetX.jpg"]
        r = requests.post(self.SOLARIMAGE_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['solarImage_id'], 38)

        r = requests.get(self.SOLARIMAGE_URL + str(resp['solarImage_id']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['solarImage_attributes'], m['solarImage_attributes'])

    def test_solarImages_index_delete(self):
        self.reset_data()

        m = {}
        r = requests.delete(self.SOLARIMAGE_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SOLARIMAGE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        solarImages = resp['solarImages']
        self.assertFalse(solarImages)

if __name__ == "__main__":
    unittest.main()
