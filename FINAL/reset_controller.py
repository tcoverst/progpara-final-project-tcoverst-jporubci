# NETID's jporubci tcoverst

import random
import json
import cherrypy
from solarSystems_library import _solarSystem_database

class ResetController(object):
    def __init__(self, mkdb=None):
        if mkdb is None:
            self.mkdb = _solarSystem_database()
        else:
            self.mkdb = mkdb

    def PUT_INDEX(self):
        output = {'result' : 'success'}
        data = json.loads(cherrypy.request.body.read().decode())
        self.mkdb.__init__()
        self.mkdb.load_solarSystems('solarSystems.dat')
        self.mkdb.load_solarImages('solarImages.dat')
        return json.dumps(output)
