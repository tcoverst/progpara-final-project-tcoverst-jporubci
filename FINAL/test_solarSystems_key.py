import unittest
import requests
import json

class TestSolarSystems(unittest.TestCase):

    SITE_URL = 'http://localhost:8080' # replace with your port number and
    print("testing for server: " + SITE_URL)
    SOLARSYSTEM_URL = SITE_URL + '/solarSystem/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_solarSystems_get_key(self):
        self.reset_data()
        solarSystem_id = 32
        r = requests.get(self.SOLARSYSTEM_URL + str(solarSystem_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['solarSystem_attributes'], ["Tethys", "533.0+/-0.7", "0.0834", "0.624", "0.0006", "0.617", "0.000103", "3570000", "0.007", "0.984+/-0.003", "0.145", "0.015", "moon of Saturn", "1684"])

    def test_solarSystems_put_key(self):
        self.reset_data()
        solarSystem_id = 18

        r = requests.get(self.SOLARSYSTEM_URL + str(solarSystem_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['solarSystem_attributes'], ["Pluto", "1188.3+/-0.8", "0.187", "7.057", "0.00651", "13.03+/-0.03", "0.0022", "17790000", "0.034", "1.854+/-0.006", "0.620", "0.063", "dwarf planet; plutino; multiple", "1930"])

        m = {}
        m['solarSystem_attributes'] = ["PlanetX", "0", "0", "0", "0", "0", "0", "0", "0", "0", "10.44", "1.065", "gas giant planet; has rings", "-"]
        r = requests.put(self.SOLARSYSTEM_URL + str(solarSystem_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SOLARSYSTEM_URL + str(solarSystem_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['solarSystem_attributes'], m['solarSystem_attributes'])

    def test_solarSystems_delete_key(self):
        self.reset_data()
        solarSystem_id = 29

        m = {}
        r = requests.delete(self.SOLARSYSTEM_URL, data = json.dumps(m))
        #print(r)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SOLARSYSTEM_URL + str(solarSystem_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')
        #self.assertEqual(resp['message'], 'solarSystem not found')

if __name__ == "__main__":
    unittest.main()
