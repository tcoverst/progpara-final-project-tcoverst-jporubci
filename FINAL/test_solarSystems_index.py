import unittest
import requests
import json

class TestSolarSystemsIndex(unittest.TestCase):

    SITE_URL = 'http://localhost:8080' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    SOLARSYSTEM_URL = SITE_URL + '/solarSystem/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_solarSystems_index_get(self):
        self.reset_data()
        r = requests.get(self.SOLARSYSTEM_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testsolarSystem = {}
        solarSystems = resp['solarSystems']
        for solarSystem in solarSystems:
            if solarSystem['solarSystem_id'] == 4:
                testsolarSystem = solarSystem

        self.assertEqual(testsolarSystem['solarSystem_attributes'], ["Saturn", "58232+/-6", "9.140", "827,130", "764", "568317+/-13", "95.162", "42612000000", "83.54", "0.6871+/-0.0002", "10.44", "1.065", "gas giant planet; has rings", "-"])

    def test_solarSystems_index_post(self):
        self.reset_data()

        m = {}
        m['solarSystem_attributes'] = ["PlanetX", "0", "0", "0", "0", "0", "0", "0", "0", "0", "10.44", "1.065", "gas giant planet; has rings", "-"]
        r = requests.post(self.SOLARSYSTEM_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['solarSystem_id'], 38)

        r = requests.get(self.SOLARSYSTEM_URL + str(resp['solarSystem_id']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['solarSystem_attributes'], m['solarSystem_attributes'])

    def test_solarSystems_index_delete(self):
        self.reset_data()

        m = {}
        r = requests.delete(self.SOLARSYSTEM_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SOLARSYSTEM_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        solarSystems = resp['solarSystems']
        self.assertFalse(solarSystems)

if __name__ == "__main__":
    unittest.main()
