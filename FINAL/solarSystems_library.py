# NETID's jporubci tcoverst

class _solarSystem_database:
    def __init__(self):
        self.solarSystems = dict()
        self.solarImages = dict()

    def load_solarSystems(self, solarSystems_file):
        f = open(solarSystems_file)
        for line in f:
            line = line.rstrip()
            str = line.split("::")
            #print(line)
            self.solarSystems[int(str[0])] = [
                str[1], str[2], str[3], str[4], str[5],
                str[6], str[7], str[8], str[9],
                str[10], str[11], str[12], str[13],
                str[14]
            ]
        #print(self.solarSystems)
        f.close()

    def load_solarImages(self, solarImages_file):
        f = open(solarImages_file)
        for line in f:
            line = line.rstrip()
            str = line.split("::")
            #print(line)
            self.solarImages[int(str[0])] = [
                str[1], str[2]
            ]
        #print(self.solarImages)
        f.close()

    def get_solarSystems(self):
        return self.solarSystems.keys()

    def get_solarImages(self):
        return self.solarImages.keys()

    def get_solarSystem(self, solarSystem_id):
        try:
            solarSystem = self.solarSystems[int(solarSystem_id)]
        except Exception as ex:
            solarSystem = None

        return solarSystem

    def get_solarImage(self, solarImage_id):
        try:
            solarImage = self.solarImages[int(solarImage_id)]
        except Exception as ex:
            solarImage = None

        return solarImage

    def set_solarSystem(self, solarSystem_id, solarSystem):
        self.solarSystems[solarSystem_id] = solarSystem

        if solarSystem_id not in self.solarSystems.keys():
            self.solarSystems[solarSystem_id] = dict()

    def set_solarImage(self, solarImage_id, solarImage):
        self.solarImages[solarImage_id] = solarImage

        if solarImage_id not in self.solarImages.keys():
            self.solarImages[solarImage_id] = dict()

    def delete_solarSystem(self, solarSystem_id):
        del(self.solarSystems[solarSystem_id])

    def delete_solarImage(self, solarImage_id):
        del(self.solarImages[solarImage_id])
