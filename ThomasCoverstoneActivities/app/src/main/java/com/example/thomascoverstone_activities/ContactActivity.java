package com.example.thomascoverstone_activities;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.thomascoverstone_activities.utilities.NetworkUtils;


import java.net.URL;
import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity {

    TextView mMessagetextView;
    Button mOpenImgButton;
    Button mOpenWebpageButton;
    Button mAPIButton;
    ImageView imageView;
    TextView mApitextView;
    String object_name;
    int object_num;
    String object_url;
    String object_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact); // connecting to xml UI

        mMessagetextView = (TextView) findViewById(R.id.tv_message);
        mOpenImgButton = (Button) findViewById(R.id.button_open_map);
        mOpenWebpageButton = (Button) findViewById(R.id.button_open_webpage);
        mAPIButton = (Button) findViewById(R.id.button_api);
        mApitextView = (TextView) findViewById(R.id.tv_api);

        mMessagetextView.setBackgroundColor(Color.argb(100,0,0, 0));
        mMessagetextView.setTextColor(Color.rgb(255,255, 255));
        mApitextView.setBackgroundColor(Color.argb(100,0,0, 0));
        mApitextView.setTextColor(Color.rgb(255,255, 255));

        Log.d("DEBUG", "ContactActivity launched");

        // grab the message from the Intent that launched me
        Intent intentThatStartedThisActivity = getIntent(); // this returns the intent that launched us
        Bundle extras = intentThatStartedThisActivity.getExtras();
        object_name = extras.getString("EXTRA_NAME");
        object_num = extras.getInt("EXTRA_NUM");
        object_url = extras.getString("EXTRA_URL");
        String incomingMessage = "Paradigms!";
        mMessagetextView.append("\n\n\n" + object_name);

        if(intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)){
            incomingMessage = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
            mMessagetextView.append("\n\n\n" + incomingMessage);

        }

        //String urlString = "https://en.wikipedia.org/wiki/Pluto";

        // open map button listener
        mOpenImgButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("DEBUG", "--------------clicked on image button");
                        openImg(object_img); // we will write this function
                    } // end of onClick - for open Map
                }
        );

        // open webpage button listener
        mOpenWebpageButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("DEBUG", "--------------clicked on web button");
                        openWebpage(object_url); // we will write this function
                    } // end of onClick - for open Webpage
                }
        );
        mAPIButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        makeNetworkQuery("/solarSystem/"); // we will write this function
                    } // end of onClick - for open api
                }
        );

    } // end of onCreate

    public void openImg(String object_img){
        //  - open map address for ND

        // todo grab search term and make network call
        String searchTerm = "/solarImage/";
        //this creates a new class object and calls its doInBackgroundMethod
        new FetchNetworkData().execute(searchTerm);


    } // end of open map

    public void openWebpage(String object_url){

        Uri webpageUri = Uri.parse(object_url);
        Intent openWebpageIntent = new Intent(Intent.ACTION_VIEW, webpageUri);
        startActivity(openWebpageIntent);

    } // end of open webpage


    //networking code starts
    public void makeNetworkQuery(String searchTerm){
        // todo grab search term and make network call
        //this creates a new class object and calls its doInBackgroundMethod
        new FetchNetworkData().execute(searchTerm);

    } // end of makeNetworkQuery

    // inner class definition
    public class FetchNetworkData extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            // this is where we will perform any network call making
            // when the response comes back from our network call
            // the this will auto trigger a method called onPostExercise

            //todo do network stuff here
            //get search term from params
            if(params.length == 0) return null;
            String searchTerm = params[0];
            Log.d("DEBUG", "----------- params[0] is " + params[0]);

            //build url object for our target url
            URL searchUrl = NetworkUtils.buildTNOUrl(params[0], Integer.toString(object_num));
            Log.d("DEBUG", "----------- searchUrl is " + searchUrl);
            //make network call
            String responseString = null;
            try {
                responseString = NetworkUtils.getResponseFromUrl(searchUrl);
            }catch(Exception e){
                e.printStackTrace();
            }
            Log.d("DEBUG", "----------- responseString is " + responseString);
            // read everything from network resonse
            // pass the entire response string to onPostExecute
            return responseString;

        } // end of doInBackground

        @Override
        protected void onPostExecute(String responseString){
            if(responseString.contains("https://upload.wikimedia")) {
                ArrayList<String> TNOjson = NetworkUtils.parseTNOJson(responseString);
                String APIimageURL = TextUtils.join("", TNOjson);
                Log.d("DEBUG", "----------- responseString is " + APIimageURL);

                imageView = findViewById(R.id.img_object);
                Glide.with(com.example.thomascoverstone_activities.ContactActivity.this).load(APIimageURL).into(imageView); //.with(View.getContext())

            }
            else{
                // this code gets triggered, when the response is received from the call we make in doInBackground
                // Todo deal with the response from the network
                ArrayList<String> TNOjson = NetworkUtils.parseTNOJson(responseString);
                for (String TNOjsonText : TNOjson) {
                    mApitextView.append("\n\n" + TNOjsonText);

                }
            }
        } //end of onPostExecute

    } //end of FetchNetworkData

} // end of class