package com.example.thomascoverstone_activities;


public class TNOProp{
    public String name;
    public int num;
    public String urlLink;

    TNOProp(String name, int num, String urlLink){
        this.name = name;
        this.num = num;
        this.urlLink = urlLink;
    }
}