package com.example.thomascoverstone_activities;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {


    //TODO: Connect with visual UI component
    TextView mResultsDisplayOne;
    TextView mResultsDisplayTwo;
    TextView mTitleDisplay;
    Button mSearchButton;
    Button mResetButton;
    Button mObjectButton;
    EditText mSearchTermEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // now this class is connected to activity_main.xml file
        mResultsDisplayOne  = (TextView)findViewById(R.id.tv_search_results);
        mResultsDisplayTwo  = (TextView)findViewById(R.id.iv_search_results);
        mTitleDisplay  = (TextView)findViewById(R.id.tv_Title);
        mSearchButton  = (Button)findViewById(R.id.button_search);
        mResetButton  = (Button)findViewById(R.id.button_reset);
        mObjectButton  = (Button)findViewById(R.id.button_object);
        mSearchTermEditText  = (EditText)findViewById(R.id.et_search_term);
        mResultsDisplayOne.setBackgroundColor(Color.argb(100,0,0, 0));
        mResultsDisplayOne.setTextColor(Color.rgb(255,255,255));
        mResultsDisplayTwo.setBackgroundColor(Color.argb(100,0,0, 0));
        mResultsDisplayTwo.setTextColor(Color.rgb(255,255,255));
        mTitleDisplay.setBackgroundColor(Color.argb(100,0,0, 0));
        mTitleDisplay.setTextColor(Color.rgb(255,255,255));
        mSearchTermEditText.setBackgroundColor(Color.argb(100,0,0, 0));
        mSearchTermEditText.setTextColor(Color.rgb(255,255,255));
        //MULTITHREADING!!!!
        Log.d("DEBUG", "Outside : " + Thread.currentThread().getName());
        Log.d("DEBUG", "Creating Runnable...");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.d("DEBUG", "Inside : " + Thread.currentThread().getName());
                String gifofsolar = "https://media-cldnry.s-nbcnews.com/image/upload/t_fit-760w,f_auto,q_auto:best/rockcms/2022-07/220712-2x1-james-webb-first-photos-ew-1212p-8122fd.gif";
                ImageView imageView = (ImageView) findViewById(R.id.img_gif_object);
                Glide.with(com.example.thomascoverstone_activities.MainActivity.this)
                        .load(gifofsolar)
                        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                        .into(new DrawableImageViewTarget(imageView));
            }
        };
        Log.d("DEBUG", "Creating Thread...");
        Thread thread = new Thread(runnable);
        Log.d("DEBUG", "Starting Thread...");
        thread.start();
        Log.d("DEBUG", "Outside : " + Thread.currentThread().getName());

        ArrayList<TNOProp> TNOs = new ArrayList<TNOProp>();
        TNOs.add(new TNOProp("Sun", 2, "https://en.wikipedia.org/wiki/Sun"));
        TNOs.add(new TNOProp("Jupiter", 3, "https://en.wikipedia.org/wiki/Jupiter"));
        TNOs.add(new TNOProp("Saturn", 4, "https://en.wikipedia.org/wiki/Saturn"));
        TNOs.add(new TNOProp("Uranus", 5, "https://en.wikipedia.org/wiki/Uranus"));
        TNOs.add(new TNOProp("Neptune", 6, "https://en.wikipedia.org/wiki/Neptune"));
        TNOs.add(new TNOProp("Earth", 7, "https://en.wikipedia.org/wiki/Earth"));
        TNOs.add(new TNOProp("Venus", 8, "https://en.wikipedia.org/wiki/Venus"));
        TNOs.add(new TNOProp("Mars", 9, "https://en.wikipedia.org/wiki/Mars"));
        TNOs.add(new TNOProp("Ganymede", 10, "https://en.wikipedia.org/wiki/Ganymede_(moon)"));
        TNOs.add(new TNOProp("Titan", 11, "https://en.wikipedia.org/wiki/Titan_(moon)"));
        TNOs.add(new TNOProp("Mercury", 12, "https://en.wikipedia.org/wiki/Mercury_(planet)"));
        TNOs.add(new TNOProp("Callisto", 13, "https://en.wikipedia.org/wiki/Callisto_(moon)"));
        TNOs.add(new TNOProp("Io", 14, "https://en.wikipedia.org/wiki/Io_(moon)"));
        TNOs.add(new TNOProp("Moon", 15, "https://en.wikipedia.org/wiki/Moon"));
        TNOs.add(new TNOProp("Europa", 16, "https://en.wikipedia.org/wiki/Europa_(moon)"));
        TNOs.add(new TNOProp("Triton", 17, "https://en.wikipedia.org/wiki/Triton_(moon)"));
        TNOs.add(new TNOProp("Pluto", 18, "https://en.wikipedia.org/wiki/Pluto"));
        TNOs.add(new TNOProp("Eris", 19, "https://en.wikipedia.org/wiki/Eris_(dwarf_planet)"));
        TNOs.add(new TNOProp("Haumea", 20, "https://en.wikipedia.org/wiki/Haumea"));
        TNOs.add(new TNOProp("Titania", 21, "https://en.wikipedia.org/wiki/Titania_(moon)"));
        TNOs.add(new TNOProp("Rhea", 22, "https://en.wikipedia.org/wiki/Rhea_(moon)"));
        TNOs.add(new TNOProp("Oberon", 23, "https://en.wikipedia.org/wiki/Oberon_(moon)"));
        TNOs.add(new TNOProp("Iapetus", 24, "https://en.wikipedia.org/wiki/Iapetus_(moon)"));
        TNOs.add(new TNOProp("Makemake", 25, "https://en.wikipedia.org/wiki/Makemake"));
        TNOs.add(new TNOProp("Gonggong", 26, "https://en.wikipedia.org/wiki/225088_Gonggong"));
        TNOs.add(new TNOProp("Charon", 27, "https://en.wikipedia.org/wiki/Charon_(moon)"));
        TNOs.add(new TNOProp("Umbriel", 28, "https://en.wikipedia.org/wiki/Umbriel_(moon)"));
        TNOs.add(new TNOProp("Ariel", 29, "https://en.wikipedia.org/wiki/Ariel_(moon)"));
        TNOs.add(new TNOProp("Dione", 30, "https://en.wikipedia.org/wiki/Dione_(moon)"));
        TNOs.add(new TNOProp("Quaoar", 31, "https://en.wikipedia.org/wiki/50000_Quaoar"));
        TNOs.add(new TNOProp("Tethys", 32, "https://en.wikipedia.org/wiki/Tethys_(moon)"));
        TNOs.add(new TNOProp("Sedna", 33, "https://en.wikipedia.org/wiki/90377_Sedna"));
        TNOs.add(new TNOProp("Ceres", 34, "https://en.wikipedia.org/wiki/Ceres_(dwarf_planet)"));
        TNOs.add(new TNOProp("Orcus", 35, "https://en.wikipedia.org/wiki/90482_Orcus"));
        TNOs.add(new TNOProp("Salacia", 36, "https://en.wikipedia.org/wiki/120347_Salacia"));
        TNOs.add(new TNOProp("2002 MS4", 37, "https://en.wikipedia.org/wiki/(307261)_2002_MS4"));
        //manipulate connected UI componet
        int valSplitter = TNOs.size();
        for(TNOProp t: TNOs){
            Log.d("DEBUG", "-----------------Object added: " + t.name);
            if (t.num <= 1+valSplitter/2) {
                mResultsDisplayOne.append(t.name + "\n\n");
            }
            else{
                mResultsDisplayTwo.append(t.name + "\n\n");
            }
        } // end of for loop
        String defaultDisplayTextOne = mResultsDisplayOne.getText().toString(); // to be used by the reset Button
        String defaultDisplayTextTwo = mResultsDisplayTwo.getText().toString(); // to be used by the reset Button

        mSearchButton.setOnClickListener(
                new View.OnClickListener() { // creates an unnamed object
                    @Override
                    public void onClick(View view) {
                        // this code is triggered when the button is clicked
                        // grab the user's search term
                        String searchTerm = mSearchTermEditText.getText().toString();

                        // compare with our List of student names
                        boolean nameFound = false;
                        for(TNOProp t: TNOs){
                            if(t.name.toLowerCase().equals(searchTerm.toLowerCase())){
                                mResultsDisplayOne.setText(t.name + "\n\n");
                                mResultsDisplayTwo.setText("\n\n");
                                nameFound = true;
                                break;
                            } // end of if
                        } // end of for

                        if(!nameFound){
                            mResultsDisplayOne.setText("Object not found.\n\n");
                            mResultsDisplayTwo.setText("\n\n");
                        }
                        // then show the result of either found name or no results
                    } // end of onClick inside of View.OnClickListener
                } // end of View.OnClickListener
        ); // end of setOnClickListener

        mResetButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("DEBUG", "--------------clicked on reset button");
                        // set the results display the original list
                        mResultsDisplayOne.setText(defaultDisplayTextOne);
                        mResultsDisplayTwo.setText(defaultDisplayTextTwo);
                    } // end of onClick
                } // end of View.OnClickListener

        ); // end of setOnClickListener
        mObjectButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("DEBUG", "--------------clicked on object button");
                        // use Intent to move to second page - contact activity
                        Class destinationActivity = ContactActivity.class; // destination
                        Intent startContactActivityIntent = new Intent(MainActivity.this, destinationActivity);
                        // it does not actually move yet

                        // message to send to next activity
                        String msg = mSearchTermEditText.getText().toString(); // "hello from page 1";
                        for(TNOProp t: TNOs){
                            if(t.name.toLowerCase().equals(msg.toLowerCase())){
                                mResultsDisplayOne.setText(t.name+ "\n\n");
                                mResultsDisplayTwo.setText("\n\n");
                                Bundle extras = new Bundle();
                                extras.putString("EXTRA_NAME", t.name);
                                extras.putInt("EXTRA_NUM", t.num);
                                extras.putString("EXTRA_URL", t.urlLink);
                                startContactActivityIntent.putExtras(extras);

                                break;
                            } // end of if
                        } // end of for


                        Log.d("DEBUG", "about to trigger switch to next activity");
                        startActivity(startContactActivityIntent); // this will actually trigger the switch to contact
                    } // end of onClick
                } // end of View.OnClickListener

        ); // end of setOnClickListener

    } // end of onCreate

}