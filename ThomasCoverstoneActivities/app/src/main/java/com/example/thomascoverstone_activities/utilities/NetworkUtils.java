package com.example.thomascoverstone_activities.utilities;

import android.os.Bundle;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.example.thomascoverstone_activities.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import android.view.View;


import javax.net.ssl.HttpsURLConnection;

public class NetworkUtils {
    protected void onCreate(Bundle savedInstanceState) {

    }
    public static URL buildTNOUrl(String addPathUrl, String TNOnum)  { //throws MalformedURLException
        //TODO - specific to country data
        String TNOUrlString = "https://e348-35-134-187-98.ngrok.io" + addPathUrl + TNOnum;//"http://10.0.2.2:8080/solarSystem/" + TNOnum;
        URL TNOUrl = null;
        try {
            TNOUrl = new URL(TNOUrlString); // this can potentially throw exceptions
        }catch(MalformedURLException e){
            Log.d("DEBUG", "Inside buildTNOUrl exception-----");

            e.printStackTrace();
        }
        return TNOUrl;
    } // end of buildTNOUrl

    public static String getResponseFromUrl(URL url) throws IOException {
        //TODO - generic to any rest call, but used on TNO data through URL
        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
        Log.d("DEBUG", "getResponseFromUrl url is -----" + url);
        try{
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A"); // special delimiter for end of massage
            boolean hasInput = scanner.hasNext();
            if(hasInput) return scanner.next(); // code should exit here
        } catch (Exception e){
            e.printStackTrace();
            Log.d("DEBUG", "Inside getResponseFromUrl exception-----");
        } finally {
            urlConnection.disconnect();
        }
        return null; //if it reaches here, then incorrect message computed


    } // end getResponseFromUrl

    public static ArrayList<String> parseTNOJson(String TNOResponseString){
        if (TNOResponseString.contains("SurfaceArea(km^2)")) {
            //TODO - specific to TNO data
            Log.d("DEBUG", "Inside parseTNOJson -----" + TNOResponseString);
            ArrayList<String> TNOjsonList = new ArrayList<String>();
            //TODO use TNOResponseString to create the arrayList

            try {
                JSONObject TNOObject = new JSONObject(TNOResponseString);
                int TNOsolarid = 0;
                String TNOname = "(Missing from API)";
                String TNOradius = "(Missing from API)";
                String TNOvolume = "(Missing from API)";
                String TNOmass = "(Missing from API)";
                String TNOsurfaceArea = "(Missing from API)";
                String TNOdensity = "(Missing from API)";
                String TNOgravity = "(Missing from API)";
                String TNOdiscovered = "(Missing from API)";
                String TNObodyType = "(Missing from API)";

                if (TNOObject.has("solarSystem_id")) {
                    TNOsolarid = TNOObject.getInt("solarSystem_id");
                }
                if (TNOObject.has("Name")) {
                    TNOname = TNOObject.getString("Name");
                }
                if (TNOObject.has("Type")) {
                    TNObodyType = TNOObject.getString("Type");
                }
                if (TNOObject.has("Density(g/cm^3)")) {
                    TNOdensity = TNOObject.getString("Density(g/cm^3)");
                }
                if (TNOObject.has("Radius(km)")) {
                    TNOradius = TNOObject.getString("Radius(km)");
                }
                if (TNOObject.has("Volume(km^3)")) {
                    TNOvolume = TNOObject.getString("Volume(km^3)");
                }
                if (TNOObject.has("Mass(kg)")) {
                    TNOmass = TNOObject.getString("Mass(kg)");
                }
                if (TNOObject.has("SurfaceArea(km^2)")) {
                    TNOsurfaceArea = TNOObject.getString("SurfaceArea(km^2)");
                }
                if (TNOObject.has("Gravity(m/s^2)")) {
                    TNOgravity = TNOObject.getString("Gravity(m/s^2)");
                }
                if (TNOObject.has("Discovered")) {
                    TNOdiscovered = TNOObject.getString("Discovered");
                }

                TNOjsonList.add(TNOname + " is ranked " + (TNOsolarid - 1) + " in terms of size in the solar system. It's is defined by its characteristics: " + TNObodyType + ". It was discovered in " + TNOdiscovered + ".\n");
                TNOjsonList.add(TNOname + " has a radius of " + TNOradius + "km.\n");
                TNOjsonList.add(TNOname + " has a volume of " + TNOvolume + "*10^9 km^3.\n");
                TNOjsonList.add(TNOname + " has a mass of " + TNOmass + "*10^21 kg.\n");
                TNOjsonList.add(TNOname + " has a surface area of " + TNOsurfaceArea + " km^2.\n");
                TNOjsonList.add(TNOname + " has a density of " + TNOdensity + " g/cm^3.\n");
                TNOjsonList.add(TNOname + " has a gravity of " + TNOgravity + " m/s^2.\n");

            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("DEBUG", "JSON parsing exception occured in the code");
            }

            //TNOjsonList.add(TNOResponseString);
            return TNOjsonList;
        }
        else{
            //TODO - specific to TNO data
            ImageView imageView;
            Log.d("DEBUG", "Inside parseTNOJson -----" + TNOResponseString);
            ArrayList<String> TNOjsonList = new ArrayList<String>();
            //TODO use TNOResponseString to create the arrayList

            try {
                JSONObject TNOObject = new JSONObject(TNOResponseString);
                int TNOsolarid = 0;
                String TNOname = "(Missing from API)";
                String TNOImage = "(Missing from API)";


                if (TNOObject.has("solarSystem_id")) {
                    TNOsolarid = TNOObject.getInt("solarSystem_id");
                }
                if (TNOObject.has("Name")) {
                    TNOname = TNOObject.getString("Name");
                }
                if (TNOObject.has("Image")) {
                    TNOImage = TNOObject.getString("Image");
                }
                TNOjsonList.add(TNOImage);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("DEBUG", "JSON parsing exception occured in the code");
            }

            //TNOjsonList.add(TNOResponseString);
            return TNOjsonList;

        }
    } // end of parseTNOJson


} // end of class
