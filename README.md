# Progpara Final Project tcoverst jporubci

Our group utilized OOP, Rest APIs, Android app development, and multi-threading to complete our space-themed app. 


## Presentation:
 
https://drive.google.com/file/d/18-7RGt9HAxcWpLhqc7Fv1VyPuKA7_aE3/view?usp=sharing
https://gitlab.com/tcoverst/progpara-final-project-tcoverst-jporubci/-/tree/main 

User Interaction:

Our server api can make GET, PUT, POST, and DELETE requests. The database used is about solar system objects. There are 36 objects with a mean radius greater than 400km in our solar system, so there are 36 objects in our database. Each object has numerous attributes, which can be called from the api. The possible commands are as follows:

| Request Type | Resource endpoint | Body | Expected response | Inner working of handler |
| ------ | ------ | ------ | ------ | ------ |
| GET | /solarSystem/ | No body | string formatted json of all objects | GET_INDEX to get all objects and attributes |
| GET | /solarSystem/32 | No body | string formatted of Tethys | GET_KEY to get all attributes of an object |
| PUT | /solarSystem/18 | ‘["Pluto", "1188.3+/-0.8", "0.187", "7.057", "0.00651", "13.03+/-0.03", "0.0022", "17790000", "0.034", "1.854+/-0.006", "0.620", "0.063", "dwarf planet; plutino; multiple", "1930"]’ | {“result”: “success”} if the operation worked | PUT_KEY updates an object entry |
| POST | /solarSystem/ | ‘["PlanetX", "0", "0", "0", "0", "0", "0", "0", "0", "0", "10.44", "1.065", "gas giant planet; has rings", "-"]’ | {“result”: “success”, “id”: 38} if the operation worked | POST_INDEX creates a new object entry and returns the new id. new id should be max+1 of all existing object ids |
| DELETE | /solarSystem/ | ‘{}’ | {“result”: “success”} if the operation worked | DELETE_INDEX clears all the objects |
| GET | /solarImage/ | No body | string formatted json of all image urls | GET_INDEX to get all objects and attributes |
| GET | /solarImage/32 | No body | image url of Tethys | GET_KEY to get all attributes of an object |
| PUT | /solarImage/18 | ‘["Pluto", "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Pluto_in_True_Color_-_High-Res.jpg/800px-Pluto_in_True_Color_-_High-Res.jpg"])’ | {“result”: “success”} if the operation worked | PUT_KEY updates an object entry |
| POST | /solarImage/ | ‘["PlanetX", "planetX.jpg"]’ | {“result”: “success”, “id”: 38} if the operation worked | POST_INDEX creates a new object entry and returns the new id. new id should be max+1 of all existing object ids |
| DELETE | /solarImage/ | ‘{}’ | {“result”: “success”} if the operation worked | DELETE_INDEX clears all the objects |
| PUT | /reset/ | No body | {“result”: “success”} if the operation worked | PUT_INDEX reloads all objects |

The necessary input to use the api can be found demonstrated in our test files. To run the test files, simply run the server, then run the test files in the terminal. To start the server, simply run the python file for the server in the terminal. This is all that is required to run the test files. To run the android app, one more step is required.
The Ngrok.exe program needs to be run. After it is started up, enter: “ngrok http 8080”. This will run the command that makes the localhost a publically available API. This is done by the program supplying an internet enabled URL link that points to our localhost. The URL is then used inside of the NetworkUtils.java. 
The Ngrok.exe will provide a new url every session. This url has to be updated in the Android Studio NetworkUtils.java code. 

## Videos:
Video 1 demo: https://drive.google.com/file/d/1uC7QkiSZ6S4eo5TQy8-rkzUKccSlpQPj/view?usp=sharing

Video 2 code review: https://drive.google.com/file/d/1YGmEJYa49lDY5TV0csZGcnhtg0OMhDr5/view?usp=sharing 

## Complexity:
For this project, we first needed to develop a server that could make API calls for a database. Then, we needed to be able to access the server from our android emulator. Finally, we needed to develop a front-end for users to interact with the data using our server. The most challenging part of the project was figuring out how to connect the android emulator to our server. The solution that we came up with was to use Ngrok. We weren’t able to connect our android emulator to our localhost using the special 10.0.2.2 address, but we were previously able to get our app to connect to an external api. This led us to convert the localhost into a publically available API, which is what Ngrok does. Ngrok acts as a pointer to our localhost. This allowed the solar system API to be available on all computers, including the android emulator. 

Additionally, the android app utilizes multithreading to display a gif of the recent JWST images while solar system objects are created, sorted, and listed on the screen.